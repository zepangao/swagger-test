package com.clubfactory.swaggerTest.server.VO;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("用户userVO")
public class UserVO {

    @ApiModelProperty(value = "用户名称", notes = "用户登录时填的名称")
    private String userName;

    @ApiModelProperty("用户年龄")
    private Long age;

    @ApiModelProperty("用户地址")
    private String address;
}
