package com.clubfactory.swaggerTest.server.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lianghaijun
 * @date 2018-11-23
 */
@RestController
public class CheckHealthController {

    @GetMapping("/check-health")
    public String checkHealth() {
        return "success";
    }

}
