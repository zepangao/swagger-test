package com.clubfactory.swaggerTest.server.controller;

import com.clubfactory.boot.toolkit.result.Result;
import com.clubfactory.swaggerTest.server.VO.UserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/example")
@Api("商家用户管理")
public class ExampleController {

    @PostMapping("/user/{userId}")
    @ApiOperation("获取用户")
    public Result<UserVO> getUser(
            @PathVariable @ApiParam("用户 Id") String userId,
            @RequestParam(required = false) @ApiParam(value = "用户名称") String userName,
            @RequestBody @ApiParam("用户对象") UserVO user
    ) {
        Result<UserVO> res = new Result<>();
        user.setAge(user.getAge() + 1);
        res.setModel(user);
        return res;
    }
}
